#include <ros/ros.h>
#include <pcl_ros/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>

#include <tf/transform_broadcaster.h>
#include <tf_conversions/tf_eigen.h>

#include <Eigen/Core>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/common/time.h>
#include <pcl/console/print.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/fpfh_omp.h>
#include <pcl/features/shot_omp.h>
#include <pcl/keypoints/uniform_sampling.h>
#include <pcl/keypoints/iss_3d.h>
#include <pcl/filters/filter.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/io/pcd_io.h>
#include <pcl/correspondence.h>
#include <pcl/recognition/cg/geometric_consistency.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/sample_consensus_prerejective.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/visualization/pcl_visualizer.h>

namespace eureca
{

// Types
typedef pcl::PointXYZ PointT;
typedef pcl::PointCloud<PointT> PointCloudT;
typedef pcl::Normal NormalT;
typedef pcl::PointCloud<NormalT> NormalCloudT;
// typedef pcl::SHOT352 FeatureT;
// typedef pcl::SHOTEstimationOMP<PointT, NormalT, FeatureT> FeatureEstimationT;
typedef pcl::FPFHSignature33 FeatureT;
typedef pcl::FPFHEstimationOMP<PointT, NormalT, FeatureT> FeatureEstimationT;
typedef pcl::PointCloud<FeatureT> FeatureCloudT;
typedef pcl::visualization::PointCloudColorHandlerCustom<PointT> ColorHandlerT;

class Perception
{
private:
  ros::NodeHandle nh_;

  PointCloudT::Ptr cloud_;
  PointCloudT::Ptr object_;
  bool new_cloud_ = false;

  ros::Publisher cloud_pub_;
  ros::Subscriber cloud_sub_;

  tf::TransformBroadcaster br_;

  // TF
  Eigen::Matrix4f transformation_;

  void pointcloudCallback(const PointCloudT::ConstPtr& msg)
  {
    *cloud_ = *msg;
    new_cloud_ = true;
  }

  void computeKeypoints(const PointCloudT::ConstPtr& cloud,
                        pcl::search::KdTree<PointT>::Ptr& tree,
                        PointCloudT::Ptr& keypoints)
  {

    pcl::PointCloud<int> keypoints_idx;


// Keypoint detector object.
pcl::UniformSampling<PointT> detector;
detector.setInputCloud(cloud);
detector.setSearchMethod(tree);
detector.setRadiusSearch(0.02); //0.02
{
  pcl::ScopeTime t("Keypoints");
  detector.compute(keypoints_idx);
  pcl::copyPointCloud(*cloud, keypoints_idx.points, *keypoints);
}

  }

  void computeFeatures(const PointCloudT::ConstPtr& keypoints,
                       const NormalCloudT::ConstPtr& normals,
                       FeatureCloudT::Ptr& features)
  {
    // Feature estimation object.
    FeatureEstimationT estimator;
    estimator.setInputCloud(keypoints);
    estimator.setInputNormals(normals);
    // The radius that defines which of the keypoint's neighbors are described.
    // If too large, there may be clutter, and if too small, not enough points
    // may
    // be found.
    estimator.setRadiusSearch(0.06); // 0.2
    {
      pcl::ScopeTime t("Features");
      estimator.compute(*features);
    }
  }

  void computeNormals(const PointCloudT::ConstPtr& keypoints,
                      const PointCloudT::ConstPtr& surface,
                      pcl::search::KdTree<PointT>::Ptr& tree,
                      NormalCloudT::Ptr& normals)
  {
    pcl::NormalEstimationOMP<PointT, NormalT> nest;
    nest.setRadiusSearch(0.06); // 0.05
    nest.setInputCloud(keypoints);
    nest.setSearchMethod(tree);
    nest.setSearchSurface(surface);
    nest.compute(*normals);
  }

  void printTranformation(const Eigen::Matrix4f& transformation)
  {
    // Print results
    pcl::console::print_info("\n");
    pcl::console::print_info("    | %6.3f %6.3f %6.3f | \n",
                             transformation(0, 0), transformation(0, 1),
                             transformation(0, 2));
    pcl::console::print_info("R = | %6.3f %6.3f %6.3f | \n",
                             transformation(1, 0), transformation(1, 1),
                             transformation(1, 2));
    pcl::console::print_info("    | %6.3f %6.3f %6.3f | \n",
                             transformation(2, 0), transformation(2, 1),
                             transformation(2, 2));
    pcl::console::print_info("\n");
    pcl::console::print_info("t = < %0.3f, %0.3f, %0.3f >\n",
                             transformation(0, 3), transformation(1, 3),
                             transformation(2, 3));
    pcl::console::print_info("\n");
  }

  void publishTranformation(const Eigen::Matrix4f& transformation)
  {
    Eigen::Matrix4d md(transformation.cast<double>());
    Eigen::Affine3d affine(md);
    tf::Transform tf;
    tf::transformEigenToTF(affine, tf);

    br_.sendTransform(tf::StampedTransform(tf, ros::Time::now(),
                                           "camera_frame_optical", "panel"));
  }

public:
  Perception(const PointCloudT::ConstPtr& object)
      : object_(new PointCloudT), cloud_(new PointCloudT)
  {
    *object_ = *object;
    cloud_sub_ = nh_.subscribe<PointCloudT>(
        "/camera/depth/points", 1, &Perception::pointcloudCallback, this);
    cloud_pub_ = nh_.advertise<PointCloudT>("/panel", 1000);
    transformation_ = Eigen::Matrix4f::Identity();
  }

  void reset() { transformation_ = Eigen::Matrix4f::Identity(); }

  double compute()
  {

    // Point clouds
    PointCloudT::Ptr scene(new PointCloudT);
    PointCloudT::Ptr scene_keypoints(new PointCloudT);
    NormalCloudT::Ptr scene_normals(new NormalCloudT);
    FeatureCloudT::Ptr scene_features(new FeatureCloudT);

    PointCloudT::Ptr object(new PointCloudT);
    PointCloudT::Ptr object_keypoints(new PointCloudT);
    NormalCloudT::Ptr object_normals(new NormalCloudT);
    FeatureCloudT::Ptr object_features(new FeatureCloudT);

    PointCloudT::Ptr object_al(new PointCloudT);

    // reset();
    // pcl::transformPointCloud(*object_, *object, transformation_);
    *object = *object_;

    // Load scene
    pcl::console::print_highlight("Loading scene...\n");
    while (!new_cloud_)
    {
      ros::spinOnce();
      sleep(1.0);
    }

    *scene = *cloud_;

    // Remove NaN
    pcl::console::print_highlight("Removing NaN...\n");
    std::vector<int> indices;
    pcl::removeNaNFromPointCloud(*scene, *scene, indices);
    pcl::removeNaNFromPointCloud(*object, *object, indices);

    // Compute KdTree
    pcl::console::print_highlight("Compute KdTree...\n");
    pcl::search::KdTree<PointT>::Ptr scene_tree(
        new pcl::search::KdTree<PointT>());
    scene_tree->setInputCloud(scene);
    pcl::search::KdTree<PointT>::Ptr object_tree(
        new pcl::search::KdTree<PointT>());
    object_tree->setInputCloud(object);

    pcl::console::print_highlight("Estimating keypoints...\n");
    computeKeypoints(scene, scene_tree, scene_keypoints);
    computeKeypoints(object, object_tree, object_keypoints);

    // Estimate normals
    pcl::console::print_highlight("Estimating normals...\n");
    computeNormals(scene_keypoints, scene, scene_tree, scene_normals);
    computeNormals(object_keypoints, object, object_tree, object_normals);

    // Estimate features
    pcl::console::print_highlight("Estimating scene features...\n");
    computeFeatures(scene_keypoints, scene_normals, scene_features);

    pcl::console::print_highlight("Estimating object features...\n");
    computeFeatures(object_keypoints, object_normals, object_features);

    // Perform alignment
    pcl::console::print_highlight("Starting alignment...\n");
    pcl::SampleConsensusPrerejective<PointT, PointT, FeatureT> align;
    align.setInputSource(object_keypoints);
    align.setSourceFeatures(object_features);
    align.setInputTarget(scene_keypoints);
    align.setTargetFeatures(scene_features);
    // align.setMaximumIterations(5000); // Number of RANSAC iterations
    align.setNumberOfSamples(
        3); // Number of points to sample for generating/prerejecting a pose
    align.setCorrespondenceRandomness(5); // Number of nearest features to use
    align.setSimilarityThreshold(
        0.6f); // Polygonal edge length similarity threshold
    align.setMaxCorrespondenceDistance(0.1); // 0.05 // Inlier threshold
    align.setInlierFraction(
        0.35f); // Required inlier fraction for accepting a pose hypothesis
    align.setEuclideanFitnessEpsilon(0.05);
    {
      pcl::ScopeTime t("Alignment");
      align.align(*object);
    }

    if (!align.hasConverged())
    {
      ROS_ERROR("Alignment failed!");
      return -1;
    }

    object->header.frame_id = "camera_frame_optical";
    cloud_pub_.publish(*object);

    Eigen::Matrix4f transformation;

    transformation = align.getFinalTransformation();

    pcl::IterativeClosestPoint<PointT, PointT> icp;
    icp.setMaximumIterations(200);
    icp.setEuclideanFitnessEpsilon(0.001); //
    icp.setInputSource(object);
    icp.setInputTarget(scene_keypoints);
    icp.setMaxCorrespondenceDistance(0.25); // 0.05
    {
      pcl::ScopeTime t("ICP");
      icp.align(*object);
    }

    if (!icp.hasConverged())
    {
      ROS_ERROR("Alignment failed!");
      return -1;
    }

    transformation = icp.getFinalTransformation() * transformation;

    // transformation_ = transformation_ * transformation;
    transformation_ = transformation;

    publishTranformation(transformation_);

    object->header.frame_id = "camera_frame_optical";
    cloud_pub_.publish(*object);

    return icp.getFitnessScore();
  }
};
}

// Align a rigid object to a scene with clutter and occlusions
int main(int argc, char** argv)
{
  ros::init(argc, argv, "perception_node");

  std::string object_filename =
      "/home/nicola/catkin_ws/src/"
      "itr-eureca/eureca_perception/data/panel.pcd";

  pcl::PointCloud<pcl::PointXYZ>::Ptr object(
      new pcl::PointCloud<pcl::PointXYZ>);

  // Load object
  ROS_INFO("Loading object...");
  if (pcl::io::loadPCDFile<pcl::PointXYZ>(object_filename, *object) < 0)
  {
    ROS_ERROR("Error loading object file!");
    return (1);
  }

  eureca::Perception perception(object);

  while (ros::ok())
  {
    double score = perception.compute();
    if (score < 0)
    {
      ROS_ERROR("Error computing alignment!");
      return (1);
    }
    ROS_INFO("Alignement score: %f", score);
    ros::spinOnce();
  }
  return (0);
}
