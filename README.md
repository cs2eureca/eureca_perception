# eureca_perception

The package has been developed within the scope of the project [EURECA](www.cleansky-eureca.eu/), funnded from the [Clean Sky](www.cleansky.eu) Joint Undertaking under the [European Union’s Horizon 2020]](https://ec.europa.eu/programmes/horizon2020/)  research and innovation programme under grant agreement nº 738039

> _The EURECA project framework is dedicated to innovate the assembly of aircraft interiors using advanced human-robot collaborative solutions. A pool of devices/frameworks will be deployed for teaming up with human operators in a human-centred assistive environment. The main benefits are the substantial improvement of ergonomics in workloads, the increase in the usability level of assembly actions, the digitalization of procedures with logs and visuals for error prevention through dedicated devices. Solutions are peculiarly designed for addressing both the working conditions and the management of the cabin-cargo installation process, such as limited maneuvering space, limited weight allowed on the cabin floor, reducing lead time and recurring costs. With this aim, EURECA will bring together research advancements spanning across design, manufacturing and control, robotized hardware, and software for the specific use-cases in the cabin-cargo final assembly._


## Panel pose estimation





The panel pose estimator relies on PCL (PointCloud Library) to process RGB-D data from the camera.  The algorithm uses the mesh of the panel to find the 6D pose of the panel respect to the robot.  The mesh (in obj format) is converted to a PointCloud and memorised as master.

The algorithm to estimate the 6D pose of the panel calculate the transformation between the master PointCloud and the panel recognised in the scene.




Pipeline:


- Voxelization of both the panel PointCloud and the scene acquired with the RGB-D camera.


- The algorithm estimate normals, and keypoints for both the panel and the scene.


- Features are estimated for each keypoint.


- Detection of the panel and pre-alignement of the PointCloud are performed using the keypoints.


- Refinement with ICP (Iterative Closest Point) algorithm using all the points.


- Extraction of the 6D pose





## Usage



### Mesh to PointCloud

The command to run the creation of a mesh from a point-cloud is:

	rosrun eureca_perception mesh2features <input_file.obj> <output_file.pcd> -leaf_size 0.001


Useful Options:

	- n_samples N : number of samples
	- leaf_size N : the XYZ lead size for voxelization


### Panel Pose estimator


	rosrun eureca_perception node





__NOTE__: the path of the master PointCloud is currently hardcoded




